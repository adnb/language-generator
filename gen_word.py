#!/usr/bin/python3
# coding: utf8
######################
# Générateur de Mots #
######################

import random
import argparse

separator = " ()\n—«»"
ponctuation = ":;,.!?\n…'’-"
queueSize = 2
taille = 3

def parse(text):
    """
    Parse un texte pour en extraire la liste des mots
    """
    word = ""
    wordsList = list()

    text = text.lower()
    text = text.replace('.',' .')
    text = text.replace(',',' ,')
    text = text.replace('…',' …')
    text = text.replace("'"," ' ")
    text = text.replace('’',' ’ ')
    text = text.replace('-',' - ')

    # Parcours du texte
    for c in text:
        # Découpage des mots
        if c in separator:
            if len(word) > 0:
                wordsList.append(word)
            word = ""
        else:
            word += c

    wordsList = list(set(wordsList))
    wordsList = [word for word in wordsList if len(word) > taille]
    return wordsList

def genNext(wordsList):
    """
    Génère le dictionnaire next
    """
    nextDict = dict()

    # Parcours du texte
    for word in wordsList:
        previousQueue = ["."]
        for char in word+'.':

            # Pour toutes les profondeurs (afin de pouvoir rétrograder la précision)
            for j in range(0,len(previousQueue)):
                previousChars = "".join(previousQueue[j:])

                if previousChars not in nextDict.keys():
                    nextDict[previousChars] = list()
                nextDict[previousChars].append(char)

            # Gestion de la mémoire
            previousQueue.append(char)
            if len(previousQueue) > queueSize:
                previousQueue.pop(0)

    return nextDict

def explore(nextDict):
    """
    Génère une phrase en parcourant la matrice des mots
    """
    rand = random.randint(0,len(nextDict['.'])-1)
    char = nextDict['.'][rand]
    previousQueue = ['.']

    # Boucle jusqu'à fin d'une phrase
    while char not in ['.']:
        # Affichage
        if previousQueue[-1] == '.':
            print(char.capitalize(), end="")
        else:
            print(char, end="")

        # Gestion de la mémoire
        previousQueue.append(char)
        if len(previousQueue) > queueSize:
            previousQueue.pop(0)

        # Recherche du char suivant
        for i in range(0,len(previousQueue)):
            previousChars = ''.join(previousQueue[i:])
            if previousChars in nextDict.keys():
                rand = random.randint(0,len(nextDict[previousChars])-1)
                char = nextDict[previousChars][rand]
                if False:
                    print("("+previousChars+")",end="")
                break
    print()

if __name__=='__main__':

    # Gestion des paramètres
    parser = argparse.ArgumentParser()
    parser.add_argument("texte", help="le texte à manger")
    parser.add_argument("-v", help="plus verbeux", action="store_true")
    parser.add_argument("-n", help="nombre de mots")
    parser.add_argument("-q", help="taille mémoire")
    parser.add_argument("-t", help="taille minimale des mots traités")
    args = parser.parse_args()

    with open(args.texte,'r') as fichier:
        text = fichier.read()

    if args.q:
        queueSize = int(args.q)

    if args.t:
        taille = int(args.t)

    # Parsage du texte
    wordsList = parse(text)
    # Génération de la fonction du mot suivant
    nextDict = genNext(wordsList)

    if args.v:
        keys = list(nextDict.keys())
        keys.sort()
        for word in keys:
            print(word,':',nextDict[word])

    # Création d'un mot
    if args.n:
        for i in range(0,int(args.n)):
            explore(nextDict)
    else:
        explore(nextDict)
