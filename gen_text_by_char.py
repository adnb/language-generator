#!/usr/bin/python3
# coding: utf8
#######################
# Générateur de texte #
#######################

import random
import argparse

queueSize = 2

def genNext(text):
    """
    Génère le dictionnaire next
    """
    nextDict = dict()
    previousQueue = ["."," "]

    # Parcours du texte
    for char in text:

        # Pour toutes les profondeurs (afin de pouvoir rétrograder la précision)
        for j in range(0,len(previousQueue)):
            previousChars = "-".join(previousQueue[j:])

            if previousChars not in nextDict.keys():
                nextDict[previousChars] = list()
            nextDict[previousChars].append(char)

        # Gestion de la mémoire
        previousQueue.append(char)
        if len(previousQueue) > queueSize:
            previousQueue.pop(0)

    return nextDict

def explore(nextDict, nb):
    """
    Génère une phrase en parcourant la matrice des mots
    """
    rand = random.randint(0,len(nextDict['.'])-1)
    char = nextDict['.'][rand]
    previousQueue = ['.']

    phrase = 0
    # Boucle jusqu'à fin d'une phrase
    while phrase < nb:
        if char in ['.','\n']:
            phrase += 1
        # Affichage
        if previousQueue[-1] == '.':
            print(char.capitalize(), end="")
        else:
            print(char, end="")

        # Gestion de la mémoire
        previousQueue.append(char)
        if len(previousQueue) > queueSize:
            previousQueue.pop(0)

        # Recherche du char suivant
        for i in range(0,len(previousQueue)):
            previousChars = '-'.join(previousQueue[i:])
            if previousChars in nextDict.keys():
                rand = random.randint(0,len(nextDict[previousChars])-1)
                char = nextDict[previousChars][rand]
                if False:
                    print("("+previousChars+")",end="")
                break

if __name__=='__main__':

    # Gestion des paramètres
    parser = argparse.ArgumentParser()
    parser.add_argument("texte", help="le texte à manger")
    parser.add_argument("-v", help="plus verbeux", action="store_true")
    parser.add_argument("-n", help="nombre de phrases")
    parser.add_argument("-q", help="taille mémoire")
    args = parser.parse_args()

    with open(args.texte,'r') as fichier:
        text = fichier.read()

    if args.q:
        queueSize = int(args.q)

    # Génération de la fonction du mot suivant
    nextDict = genNext(text)

    if args.v:
        keys = list(nextDict.keys())
        keys.sort()
        for word in keys:
            print(word,':',nextDict[word])

    # Création d'un mot
    if args.n:
        explore(nextDict,int(args.n))
    else:
        explore(nextDict,5)
