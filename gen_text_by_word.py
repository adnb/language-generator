#!/usr/bin/python3
# coding: utf8
#########################
# Générateur de phrases #
#########################

import random
import argparse

separator = " ()\n—«»_\""
ponctuation = ":;,.!?\n…'’-"
queueSize = 2

def parse(text):
    """
    Parse un texte pour en extraire la liste des mots
    """
    word = ""
    wordsList = list()

    text = text.lower()
    text = text.replace('.',' .')
    text = text.replace(',',' ,')
    text = text.replace('…',' …')
    text = text.replace("'"," ' ")
    text = text.replace('’',' ’ ')
    text = text.replace('-',' - ')

    # Parcours du texte
    for c in text:
        # Découpage des mots
        if c in separator:
            if len(word) > 0:
                wordsList.append(word)
            word = ""
        else:
            word += c

    return wordsList

def genNext(wordsList):
    """
    Génère le dictionnaire next
    """
    word = "."
    previousQueue = ["."]
    nextDict = dict()

    # Parcours du texte
    for word in wordsList:

        # Pour toutes les profondeurs (afin de pouvoir rétrograder la précision)
        for j in range(0,len(previousQueue)):
            previousWords = "-".join(previousQueue[j:])

            if previousWords not in nextDict.keys():
                nextDict[previousWords] = list()
            nextDict[previousWords].append(word)

        # Gestion de la mémoire
        # On oublie avant la ponctuation… ou pas
        if word in ponctuation and False:
            previousQueue = [word]
        else:
            previousQueue.append(word)
            if len(previousQueue) > queueSize:
                previousQueue.pop(0)

    return nextDict

def explore(nextDict, nb):
    """
    Génère une phrase en parcourant la matrice des mots
    """
    rand = random.randint(0,len(nextDict['.'])-1)
    word = nextDict['.'][rand]
    previousQueue = ['.']

    phrase = 0
    # Boucle jusqu'à fin d'une phrase
    while phrase < nb:
        if word in ['.','!','?']:
            phrase += 1
        # Affichage
        if word in ['.',',',"'",'’','-']:
            print('\b', end="")
        if previousQueue[-1] == '.':
            print(word.capitalize(), end=" ")
        else:
            if previousQueue[-1] in ["'",'’','-']:
                print('\b'+word, end=" ")
            else:
                print(word, end=" ")

        # Gestion de la mémoire
        previousQueue.append(word)
        if len(previousQueue) > queueSize:
            previousQueue.pop(0)

        # Recherche du mot suivant
        for i in range(0,len(previousQueue)):
            previousWords = '-'.join(previousQueue[i:])
            if previousWords in nextDict.keys():
                rand = random.randint(0,len(nextDict[previousWords])-1)
                word = nextDict[previousWords][rand]
                if False:
                    print("("+previousWords+")",end="")
                break

    print('\b.')

if __name__=='__main__':

    # Gestion des paramètres
    parser = argparse.ArgumentParser()
    parser.add_argument("texte", help="le texte à manger")
    parser.add_argument("-v", help="plus verbeux", action="store_true")
    parser.add_argument("-n", help="nombre de phrases")
    parser.add_argument("-q", help="taille mémoire")
    args = parser.parse_args()

    with open(args.texte,'r') as fichier:
        text = fichier.read()

    if args.q:
        queueSize = int(args.q)

    # Parsage du texte
    wordsList = parse(text)
    # Génération de la fonction du mot suivant
    nextDict = genNext(wordsList)

    if args.v:
        keys = list(nextDict.keys())
        keys.sort()
        for word in keys:
            print(word,':',nextDict[word])

    # Création de n phrases
    if args.n:
        explore(nextDict,int(args.n))
    else:
        explore(nextDict,10)
