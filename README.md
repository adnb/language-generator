# Language Generator

A python script to reproduce words and texts according to the probability in a text.

* gen_word.py -- Generate a random word according to the probability of character succession.
* gen_text_by_char.py -- Generate a random text according to the probability of character succession.
* gen_text_by_word.py -- Generate a random text according to the probability of word succession.

```
usage: gen_word.py [-h] [-v] [-n N] [-q Q] [-t T] text

positional arguments:
  text        the reference text to build probability mapping

optional arguments:
  -h, --help  show this help message and exit
  -v          verbose mode (very verbose)
  -n N        number of words to print
  -q Q        memory deepness to compute probability
  -t T        minimal size of a word considered in the reference text
```

```
usage: gen_text_by_word.py [-h] [-v] [-n N] [-q Q] text

positional arguments:
  text        the reference text to build probability mapping

optional arguments:
  -h, --help  show this help message and exit
  -v          verbose mode (very verbose)
  -n N        number of sentences to print
  -q Q        memory deepness to compute probability
```
